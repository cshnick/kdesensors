##cmake
run in bash:
```bash
export CC=gcc-7
export CXX=g++-7
cmake -DCMAKE_BUILD_TYPE=Debug \
      ../kdesensors
```