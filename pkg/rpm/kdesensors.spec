Name:           kdesensors

%define major 1
%define minor 0
%define revision 5
%define version %{major}.%{minor}.%{revision}

Version:        %{version}
Release:        leap
Summary:        Yet another kde sensors information 
Source:         https://bitbucket.org/cshnick/kdesensors/get/v%{version}.tar.bz2
URL:            https://bitbucket.org/cshnick/kdesensors/

Group:          User Interface/Desktops
License:        MIT-like 

BuildRequires: gcc > 5
BuildRequires: gcc-c++ > 5
BuildRequires: cmake >= 3.6
BuildRequires: extra-cmake-modules >= 5.26
BuildRequires: libQt5Core-devel >= 5.5.0
BuildRequires: libQt5Network-devel >= 5.5.0
BuildRequires: plasma-framework-devel >= 5.18.0
BuildRequires: libsensors4-devel

Requires:      libsensors4

%define archive_name "cshnick-kdesensors-41e35e03f111"

%description
kdesensors package containing c++ wrapper for c sensors, kde dataengine for sensors and
top panel plasmoid.

%prep
%setup -n %{archive_name}

%build
mkdir build
cd build
cmake \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DPLUGIN_PATH=/usr/lib64/sensors_ex/plugins \
    ..
%make_jobs
echo finished

%install
cd build
%make_install
#Create symlinks
cd %{buildroot}/%{_libdir}
ln -s libsensorsEx++.so libsensorsEx++.so.%{major}
ln -s libsensorsEx++.so libsensorsEx++.so.%{major}.%{minor}
ln -s libsensorsEx++.so libsensorsEx++.so.%{major}.%{minor}.%{revision}

%package dataengine
Summary: KF5 dataengine, measuring sensor data at runtime
Requires: libsensorsEx++ >= %{version}
%description dataengine
KF5 dataengine, measuring sensor data at runtime

%package plasmoid
Summary: Top panel simple plasmod to show only helpful info
Requires: libsensorsEx++ >= %{version}
Requires: %{name}-dataengine >= %{version}
%description plasmoid
Top panel simple plasmod to show only helpful info

%package test
Summary: Application to check sensor temps in terminal. Users can use c version instead
Requires: libsensorsEx++ >= %{version}
%description test
Application to check sensor temps in terminal. Users can use c version instead

%package -n libsensorsEx++
Version: %{version}
Summary:  C++ wrapper for libsensors4 c code
%description -n libsensorsEx++
C++ wrapper for libsensors4 c code.

%package -n libsensorsEx++-plugin-nvidia
Version: %{version}
Summary:  Nvidia sensor from nvidia_ml.so
Requires: libsensorsEx++ >= %{version}
%description -n libsensorsEx++-plugin-nvidia
Nvidia sensor from nvidia_ml.so

%files dataengine
%defattr(0755,root,root)
%attr(0644,root,root) %{_libdir}/qt5/plugins/plasma/dataengine/sensors_ex.so
%attr(0644,root,root) %{_datadir}/kservices5/plasma-dataengine-sensors_ex.desktop
%dir %{_libdir}/qt5/plugins/plasma/dataengine
%dir %{_libdir}/qt5/plugins/plasma
%dir %{_datadir}/kservices5

%files plasmoid
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/config/config.qml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/config/main.xml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/ui/CompactWidget.qml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/ui/CustomCombo.qml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/ui/MainWidget.qml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/ui/SettingsPage.qml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/ui/configGeneral.qml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/ui/main.qml
%attr(0644,root,root) %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/metadata.desktop
%dir %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/config
%dir %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents
%dir %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex
%dir %{_datadir}/plasma/plasmoids/org.cshnick.sensors_ex/contents/ui
%dir %{_datadir}/plasma/plasmoids

%files test
%attr(0755,root,root) %{_bindir}/sensorspp_test

%files -n libsensorsEx++
%defattr(0755,root,root) 
%attr(0644,root,root) %{_libdir}/libsensorsEx++.so
%{_libdir}/libsensorsEx++.so.%{major}
%{_libdir}/libsensorsEx++.so.%{major}.%{minor}
%{_libdir}/libsensorsEx++.so.%{major}.%{minor}.%{revision}

%files -n libsensorsEx++-plugin-nvidia
%attr(0644,root,root) %{_libdir}/sensors_ex/plugins/libchip_plugin_nvidia.so
%dir %{_libdir}/sensors_ex/plugins
%dir %{_libdir}/sensors_ex




