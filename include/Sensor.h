#pragma once

#include <memory>
#include <string>
#include <vector>
#include <ostream>
#include <unordered_map>

#define DECLARE_SENSOR_EXEPT(ClassName) \
	class ClassName : public std::runtime_error { \
	public: \
		ClassName(const std::string &what_arg) : std::runtime_error(what_arg) {} \
		ClassName(const char *what_arg) : std::runtime_error(what_arg) {} \
	}

namespace Private {
	class SensorPrivate;
} //namespace Private

namespace Sensorspp {

DECLARE_SENSOR_EXEPT(InitializationError);
DECLARE_SENSOR_EXEPT(GetLabelError);
DECLARE_SENSOR_EXEPT(GetValueError);
DECLARE_SENSOR_EXEPT(ParseChipNameError);
DECLARE_SENSOR_EXEPT(NVMLError);

typedef enum sensors_feature_copy {
	SENSORS_FEATURE_IN		= 0x00,
	SENSORS_FEATURE_FAN		= 0x01,
	SENSORS_FEATURE_TEMP		= 0x02,
	SENSORS_FEATURE_POWER		= 0x03,
	SENSORS_FEATURE_ENERGY		= 0x04,
	SENSORS_FEATURE_CURR		= 0x05,
	SENSORS_FEATURE_HUMIDITY	= 0x06,
	SENSORS_FEATURE_MAX_MAIN,
	SENSORS_FEATURE_VID		= 0x10,
	SENSORS_FEATURE_INTRUSION	= 0x11,
	SENSORS_FEATURE_MAX_OTHER,
	SENSORS_FEATURE_BEEP_ENABLE	= 0x18,
	SENSORS_FEATURE_MAX,
} sensors_feature_copy;

typedef enum print_type {
	JSON = 0x00,
	XML,
	RAW
} print_type;


struct SubSensor {
	std::string name;
	double value;
	std::string unit;

	friend std::ostream& operator<<(std::ostream& os, const SubSensor& print);
};
using SubSensors = std::vector<SubSensor>;
std::ostream& operator<<(std::ostream& os, const SubSensor& print);

struct SubAlarm {
	std::string name;
	double value;

	friend std::ostream& operator<<(std::ostream& os, const SubAlarm& print);
};
using SubAlarms = std::vector<SubAlarm>;
std::ostream& operator<<(std::ostream& os, const SubAlarm& print);

struct Feature {
	virtual int type() = 0;
	virtual void serialize(std::ostream &ostr) const = 0;
	virtual ~Feature() {}
};
using FeaturePtr = std::shared_ptr<Feature>;

struct Temp : public Feature {
	int type() {return SENSORS_FEATURE_TEMP;}
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;
	std::string sensor;

	SubSensors subsensors;
	SubAlarms subalarms;
};
using Temps = std::vector<Temp>;
std::ostream& operator<<(std::ostream& os, const Temp& print);
std::ostream& operator<<(std::ostream& os, const Temps& print);

struct In : public Feature {
	int type() {return SENSORS_FEATURE_IN;}
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;

	SubSensors subsensors;
	SubAlarms subalarms;
};

struct Fan : public Feature {
	int type() {return SENSORS_FEATURE_FAN;}
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;

	double min;
	double max;
	double div;

	bool alarm;
};

struct Vid : public Feature {
	int type() {return SENSORS_FEATURE_VID; }
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;
};

struct BeepEnable : public Feature {
	int type() {return SENSORS_FEATURE_BEEP_ENABLE; }
	void serialize(std::ostream &os) const override;

	std::string label;
	std::string value;
};

struct Power : public Feature {
	int type() {return SENSORS_FEATURE_POWER;}
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;
	std::string unit;

	SubSensors subsensors;
	SubAlarms subalarms;
};

struct Energy : public Feature {
	int type() {return SENSORS_FEATURE_ENERGY;}
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;
	std::string unit;
};

struct Curr : public Feature {
	int type() {return SENSORS_FEATURE_CURR;}
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;

	SubSensors subsensors;
	SubAlarms subalarms;
};

struct Intrusion : public Feature {
	int type() {return SENSORS_FEATURE_INTRUSION;}
	void serialize(std::ostream &os) const override;

	std::string label;
	std::string value;
};

struct Humidity : public Feature {
	int type() {return SENSORS_FEATURE_HUMIDITY;}
	void serialize(std::ostream &os) const override;

	std::string label;
	double value;
};

using Features = std::vector<FeaturePtr>;

struct Chip {
	std::string name;
	std::string prefix;
	//sensors_bus_id bus;
	int addr;
	std::string path;
	Features features;
	//Copy from features for performance reasons
	Temps temps;
	friend std::ostream& operator<<(std::ostream& os, const Chip& print);
};
using Chips = std::vector<Chip>;
std::ostream& operator<<(std::ostream& os, const Chip& print);

class Sensor {
public:
	Sensor();
	~Sensor();
	void Refresh(const char *chipstring);
	//More quick solution. Update just temperature
	void RefreshTemp(const char *chipstring);
	void Serialize(std::ostream &os) const;
	Chips AllChips() const;
	std::string PluginsPath() const;
	friend std::ostream& operator<<(std::ostream& os, const Sensor& print);

private:
	friend class Private::SensorPrivate;
	std::unique_ptr<Private::SensorPrivate> d;
};
std::ostream& operator<<(std::ostream& os, const Sensor& print);

} //namespace Sensorspp

