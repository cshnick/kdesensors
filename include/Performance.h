#pragma once

#include <functional>
#include <chrono>
#include <iostream>

#include "Common.h"

namespace Sensorspp {

template<typename Fn>
unsigned performance(Fn &&fn) {
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	fn();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	return static_cast<unsigned int>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count());
}

template<typename Fn>
void performance_log(const std::string &comment, Fn &&fn) {
	std::cout <<  comment << ", duration: " << performance(std::forward<Fn>(fn)) << std::endl;
}

} // namespace Sensorspp
