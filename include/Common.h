#pragma once

#include <tuple>
#include <memory>
#include <experimental/filesystem>
#include <iostream>

#include "Sensor.h"

namespace Sensorspp {

namespace Name {
inline static const char *nvidia_nvml = "nvidia-nvml";
} //namespace Name

namespace fs = std::experimental::filesystem;

class ChipEx {
public:
    virtual Chip chip() = 0;
};

template<typename InterfaceBase>
class PluginBase;

template<typename T>
using PluginBasePtr = std::unique_ptr<PluginBase<T>>;

template<typename InterfaceBase>
class PluginBase : public InterfaceBase {
public:
    virtual std::string name() = 0;
    virtual std::string ownerinfo() = 0;
};

using ChipPluginBase = PluginBase<ChipEx>;
using ChipPluginBasePtr = PluginBasePtr<ChipEx>;
using ChipPluginsBase = std::vector<ChipPluginBasePtr>;

template<typename PluginType>
class PluginLoader {
public:
    void load(const fs::path &path);
    const ChipPluginsBase &plugins() const {return plugins_;}
private:
    ChipPluginsBase plugins_;
};

template<typename T>
using CreateObjectFunc = T *(*)();
using CreateChipPluginFunc = CreateObjectFunc<ChipPluginBase>;

std::tuple<CreateChipPluginFunc, bool> readSymbols(const std::string &name);

template <typename PluginType>
void PluginLoader<PluginType>::load(const fs::path &path) {
    if (!fs::is_directory(path)) return;
    for (auto &iter : fs::directory_iterator(path)) {
        auto [func, success] = readSymbols(iter.path().string());
        if (success) {
            plugins_.emplace_back(func());
        }
    }
}

} // namespace Sensorspp
