TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += src/cpp/main.cpp
RESOURCES += src/res/qml.qrc

QML_IMPORT_PATH += /usr/lib64/qt5/qml
DEFINES += QT_DEPRECATED_WARNINGS

OBJECTS_DIR=obj
MOC_DIR=moc

DEFINES += QPM_INIT\\(E\\)=\"E.addImportPath(QStringLiteral(\\\"qrc:/\\\"));\"

include(material/material.pri)
