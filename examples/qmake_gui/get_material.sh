#!/bin/bash

if [[ ! -d material ]] ; then
    git clone git@github.com:papyros/qml-material.git material
    rm -rf material/.git
    sed -i 's:property alias theme:property alias wntheme:g' ./material/src/window/ApplicationWindow.qml
    sed -i 's:property alias theme:property alias wntheme:g' ./material/src/window/MainView.qml
    sed -i 's:property alias theme:property alias wntheme:g' ./material/src/window/Window.qml
else
    echo material exists, nothing to download
fi
