import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Layouts 1.2 as L
import Qt.labs.settings 1.0 as S

import Material 0.3 as Papyros
import Material.ListItems 0.1 as ListItem

import org.kde.plasma.core 2.0 as Core


Papyros.ApplicationWindow {
    visible: true
    width: 360
    height: 480
    title: qsTr("Kde sensors UI")

    property alias cpuSensor: widgetSettings.cpuSensor
    property alias gpuSensor: widgetSettings.gpuSensor

    onCpuSensorChanged: {
        console.log("Cpu sensor changed to " + cpuSensor)
    }

    wntheme {
        primaryColor: "#00aaff"
        accentColor: "#ffaaff"
        tabHighlightColor: "#ffffef"
        backgroundColor: "#2d3436"
    }

    S.Settings {
        id: globalSettings
        property var sources: ({})
        property string cpuSensor : ''
        property string gpuSensor : ''
    }

    Connections {
        target: widgetSettings
        onCpuSensorActivated: {
            globalSettings.cpuSensor = newCpuSensor
        }
        onGpuSensorActivated: {
            globalSettings.gpuSensor = newGpuSensor
        }
    }

    Core.DataSource {
        id: dataSource
        engine: "org.cshnick.sensors_ex"
        connectedSources: sources
        interval: 1000
        Component.onCompleted: {
            console.log("Sources changed")
            console.log(sources.length)
        }
    }

    initialPage: Papyros.Page {
        id: mainPage

        backAction: navDrawer.action
        Papyros.NavigationDrawer {
            id: navDrawer
            //Rectangle {anchors.fill: parent; color: '#2d3436'}
            Rectangle {anchors.fill: parent; color: '#eff0f1'}
            Flickable {
                id: flickable
                anchors.fill: parent
                SettingsPage {
                    id: widgetSettings

                    sources: dataSource.sources
                    anchors.fill: parent
                    anchors.margins : 4
                }
            }
        }
        L.ColumnLayout {
            anchors.fill: parent
            Loader {
                id: _ldr
                property var _cpuSensorData: cpuSensor && dataSource.data[cpuSensor] || ({})
                property var _gpuSensorData: gpuSensor && dataSource.data[gpuSensor] || ({})
                L.Layout.preferredWidth: 50
                L.Layout.preferredHeight: 50
                L.Layout.margins: 4
                sourceComponent: compactRepresentation
            }
            MainWidget {
                cpuSensorData: cpuSensor && dataSource.data[cpuSensor] || ({})

                width: 100
                height: 50
                L.Layout.fillWidth: true
                L.Layout.fillHeight: true
                L.Layout.margins: 4

                visible: true
            }
        }
    }

    Component {
        id: compactRepresentation
        L.ColumnLayout {
            anchors.fill : parent
            CompactWidget {
                cpuTemp: _cpuSensorData &&
                         _cpuSensorData.temp &&
                         _cpuSensorData.temp[0] &&
                         parseInt(_cpuSensorData.temp[0].value) ||
                         'n/a'
                gpuTemp: _gpuSensorData &&
                         _gpuSensorData.temp &&
                         _gpuSensorData.temp[0] &&
                         parseInt(_gpuSensorData.temp[0].value) ||
                         'n/a'
                L.Layout.maximumWidth: 50
                L.Layout.preferredHeight: 50
                height: 50
                width: 50
            }
        }
    }
}
