#include <iostream>
#include <algorithm>

#include <ctime>
#include <thread>
#include <unistd.h>

#include "Sensor.h"
#include "Performance.h"

using namespace Sensorspp;
using namespace std;
using namespace std::literals;

int main(int argc, char **argv) {
    cout << "Started test..." << endl;

    Sensor sensor;

    cout << "Attempting to load plugins from " << sensor.PluginsPath() << endl;

    performance_log("Init sensor", [&]() {
        sensor.Refresh(nullptr);
    });

    cout << sensor << endl;

    performance_log("Refresh", [&]() {
        sensor.Refresh(nullptr);
    });

    performance_log("RefreshTemp", [&]() {
        sensor.RefreshTemp(nullptr);
    });

    for (int i = 0; i < 600; i++) {
        for (auto &chip : sensor.AllChips()) {
            performance_log("\t..." + chip.name, [&]() {
                sensor.RefreshTemp(chip.name.c_str());
            });
        }
        cout << "Updated sensor: " << endl;
        for (auto &chip : sensor.AllChips()) {
            if (chip.temps.empty()) continue;
            cout << chip.name << " -- " << chip.temps[0].value << endl;
        }
        this_thread::sleep_for(2s);
    }

    cout << "Finished test..." << endl;

    return 0;
}
