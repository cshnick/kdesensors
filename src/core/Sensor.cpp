#include "Sensor.h"

#include <string.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <functional>

#include "Common.h"

#include <sensors/sensors.h>
#include <sensors/error.h>

#include "Common.h"

namespace Private {

/*
 * Retrieved subfeatures
 */
struct sensor_subfeature_data {
	double value;		/* Subfeature value. Not used for alarms. */
	const char *name;	/* Subfeature name */
	const char *unit;	/* Unit to be displayed for this subfeature.
				   This field is optional. */
};

/*
 * Subfeature data structure. Used to create a table of implemented subfeatures
 * for a given feature.
 */
struct sensor_subfeature_list {
	int subfeature;
	const struct sensor_subfeature_list *exists;
				/* Complementary subfeatures to be displayed
				   if subfeature exists */
	int alarm;		/* true if this is an alarm */
	const char *name;	/* subfeature name to be printed */
};


const char hyst_str[] = "hyst";

const struct sensor_subfeature_list temp_min_sensors[] = {
	{ SENSORS_SUBFEATURE_TEMP_MIN_HYST, NULL, 0, hyst_str },
	{ -1, NULL, 0, NULL }
};

const struct sensor_subfeature_list temp_lcrit_sensors[] = {
	{ SENSORS_SUBFEATURE_TEMP_LCRIT_HYST, NULL, 0, hyst_str },
	{ -1, NULL, 0, NULL }
};

const struct sensor_subfeature_list temp_max_sensors[] = {
	{ SENSORS_SUBFEATURE_TEMP_MAX_HYST, NULL, 0, hyst_str },
	{ -1, NULL, 0, NULL }
};

const struct sensor_subfeature_list temp_crit_sensors[] = {
	{ SENSORS_SUBFEATURE_TEMP_CRIT_HYST, NULL, 0, hyst_str },
	{ -1, NULL, 0, NULL }
};

const struct sensor_subfeature_list temp_emergency_sensors[] = {
	{ SENSORS_SUBFEATURE_TEMP_EMERGENCY_HYST, NULL, 0,
	    hyst_str },
	{ -1, NULL, 0, NULL }
};

const struct sensor_subfeature_list temp_sensors[] = {
	{ SENSORS_SUBFEATURE_TEMP_ALARM, NULL, 1, NULL },
	{ SENSORS_SUBFEATURE_TEMP_LCRIT_ALARM, NULL, 1, "LCRIT" },
	{ SENSORS_SUBFEATURE_TEMP_MIN_ALARM, NULL, 1, "LOW" },
	{ SENSORS_SUBFEATURE_TEMP_MAX_ALARM, NULL, 1, "HIGH" },
	{ SENSORS_SUBFEATURE_TEMP_CRIT_ALARM, NULL, 1, "CRIT" },
	{ SENSORS_SUBFEATURE_TEMP_EMERGENCY_ALARM, NULL, 1, "EMERGENCY" },
	{ SENSORS_SUBFEATURE_TEMP_MIN, temp_min_sensors, 0, "low" },
	{ SENSORS_SUBFEATURE_TEMP_MAX, temp_max_sensors, 0, "high" },
	{ SENSORS_SUBFEATURE_TEMP_LCRIT, temp_lcrit_sensors, 0, "crit low" },
	{ SENSORS_SUBFEATURE_TEMP_CRIT, temp_crit_sensors, 0, "crit" },
	{ SENSORS_SUBFEATURE_TEMP_EMERGENCY, temp_emergency_sensors, 0,
	    "emerg" },
	{ SENSORS_SUBFEATURE_TEMP_LOWEST, NULL, 0, "lowest" },
	{ SENSORS_SUBFEATURE_TEMP_HIGHEST, NULL, 0, "highest" },
	{ -1, NULL, 0, NULL }
};
#define ARRAY_SIZE(arr)	(int)(sizeof(arr) / sizeof((arr)[0]))
#define NUM_TEMP_ALARMS		6
#define NUM_TEMP_SENSORS	(ARRAY_SIZE(temp_sensors) \
				 + ARRAY_SIZE(temp_max_sensors) \
				 + ARRAY_SIZE(temp_crit_sensors) \
				 + ARRAY_SIZE(temp_emergency_sensors) \
				 - NUM_TEMP_ALARMS - 4)

static const struct sensor_subfeature_list voltage_sensors[] = {
	{ SENSORS_SUBFEATURE_IN_ALARM, NULL, 1, NULL },
	{ SENSORS_SUBFEATURE_IN_LCRIT_ALARM, NULL, 1, "LCRIT" },
	{ SENSORS_SUBFEATURE_IN_MIN_ALARM, NULL, 1, "MIN" },
	{ SENSORS_SUBFEATURE_IN_MAX_ALARM, NULL, 1, "MAX" },
	{ SENSORS_SUBFEATURE_IN_CRIT_ALARM, NULL, 1, "CRIT" },
	{ SENSORS_SUBFEATURE_IN_LCRIT, NULL, 0, "crit min" },
	{ SENSORS_SUBFEATURE_IN_MIN, NULL, 0, "min" },
	{ SENSORS_SUBFEATURE_IN_MAX, NULL, 0, "max" },
	{ SENSORS_SUBFEATURE_IN_CRIT, NULL, 0, "crit max" },
	{ SENSORS_SUBFEATURE_IN_AVERAGE, NULL, 0, "avg" },
	{ SENSORS_SUBFEATURE_IN_LOWEST, NULL, 0, "lowest" },
	{ SENSORS_SUBFEATURE_IN_HIGHEST, NULL, 0, "highest" },
	{ -1, NULL, 0, NULL }
};


#define NUM_IN_ALARMS	5
#define NUM_IN_SENSORS	(ARRAY_SIZE(voltage_sensors) - NUM_IN_ALARMS - 1)

static const struct sensor_subfeature_list current_sensors[] = {
	{ SENSORS_SUBFEATURE_CURR_ALARM, NULL, 1, NULL },
	{ SENSORS_SUBFEATURE_CURR_LCRIT_ALARM, NULL, 1, "LCRIT" },
	{ SENSORS_SUBFEATURE_CURR_MIN_ALARM, NULL, 1, "MIN" },
	{ SENSORS_SUBFEATURE_CURR_MAX_ALARM, NULL, 1, "MAX" },
	{ SENSORS_SUBFEATURE_CURR_CRIT_ALARM, NULL, 1, "CRIT" },
	{ SENSORS_SUBFEATURE_CURR_LCRIT, NULL, 0, "crit min" },
	{ SENSORS_SUBFEATURE_CURR_MIN, NULL, 0, "min" },
	{ SENSORS_SUBFEATURE_CURR_MAX, NULL, 0, "max" },
	{ SENSORS_SUBFEATURE_CURR_CRIT, NULL, 0, "crit max" },
	{ SENSORS_SUBFEATURE_CURR_AVERAGE, NULL, 0, "avg" },
	{ SENSORS_SUBFEATURE_CURR_LOWEST, NULL, 0, "lowest" },
	{ SENSORS_SUBFEATURE_CURR_HIGHEST, NULL, 0, "highest" },
	{ -1, NULL, 0, NULL }
};

#define NUM_CURR_ALARMS		5
#define NUM_CURR_SENSORS	(ARRAY_SIZE(current_sensors) - NUM_CURR_ALARMS - 1)

struct scale_table {
	double upper_bound;
	const char *unit;
};

static void scale_value(double *value, const char **prefixstr)
{
	double abs_value = fabs(*value);
	double divisor = 1e-9;
	static struct scale_table prefix_scales[] = {
		{1e-6, "n"},
		{1e-3, "u"},
		{1,    "m"},
		{1e3,   ""},
		{1e6,  "k"},
		{1e9,  "M"},
		{0,    "G"}, /* no upper bound */
	};
	struct scale_table *scale = prefix_scales;

	if (abs_value == 0) {
		*prefixstr = "";
		return;
	}

	while (scale->upper_bound && abs_value > scale->upper_bound) {
		divisor = scale->upper_bound;
		scale++;
	}

	*value /= divisor;
	*prefixstr = scale->unit;
}

static const struct sensor_subfeature_list power_common_sensors[] = {
	{ SENSORS_SUBFEATURE_POWER_ALARM, NULL, 1, NULL },
	{ SENSORS_SUBFEATURE_POWER_MAX_ALARM, NULL, 1, "MAX" },
	{ SENSORS_SUBFEATURE_POWER_CRIT_ALARM, NULL, 1, "CRIT" },
	{ SENSORS_SUBFEATURE_POWER_CAP_ALARM, NULL, 1, "CAP" },
	{ SENSORS_SUBFEATURE_POWER_MAX, NULL, 0, "max" },
	{ SENSORS_SUBFEATURE_POWER_CRIT, NULL, 0, "crit" },
	{ SENSORS_SUBFEATURE_POWER_CAP, NULL, 0, "cap" },
	{ -1, NULL, 0, NULL }
};

static const struct sensor_subfeature_list power_inst_sensors[] = {
	{ SENSORS_SUBFEATURE_POWER_INPUT_LOWEST, NULL, 0, "lowest" },
	{ SENSORS_SUBFEATURE_POWER_INPUT_HIGHEST, NULL, 0, "highest" },
	{ SENSORS_SUBFEATURE_POWER_AVERAGE, NULL, 0, "avg" },
	{ SENSORS_SUBFEATURE_POWER_AVERAGE_LOWEST, NULL, 0, "avg lowest" },
	{ SENSORS_SUBFEATURE_POWER_AVERAGE_HIGHEST, NULL, 0, "avg highest" },
	{ SENSORS_SUBFEATURE_POWER_AVERAGE_INTERVAL, NULL, 0,
		"interval" },
	{ -1, NULL, 0, NULL }
};

static const struct sensor_subfeature_list power_avg_sensors[] = {
	{ SENSORS_SUBFEATURE_POWER_AVERAGE_LOWEST, NULL, 0, "lowest" },
	{ SENSORS_SUBFEATURE_POWER_AVERAGE_HIGHEST, NULL, 0, "highest" },
	{ SENSORS_SUBFEATURE_POWER_AVERAGE_INTERVAL, NULL, 0,
		"interval" },
	{ -1, NULL, 0, NULL }
};

struct SensorRaiiHelper {
	SensorRaiiHelper() {
		int err = sensors_init(nullptr);
		if (err) {
			throw Sensorspp::InitializationError("Error on sensors_init with nullptr");
		}
	}
	~SensorRaiiHelper() {sensors_cleanup();}
};
struct SensorsParseChipNameRaiiHelper {
	SensorsParseChipNameRaiiHelper(const char *chipstring) {
		if (sensors_parse_chip_name(chipstring, &parsed_chip)) {
			throw Sensorspp::ParseChipNameError(std::string("Parse error in chip name ") + chipstring);
		}
	}
	~SensorsParseChipNameRaiiHelper() {
		sensors_free_chip_name(&parsed_chip);
	}
	sensors_chip_name parsed_chip;
};
class SensorPrivate {
	friend class Sensorspp::Sensor;
public:
	SensorPrivate(Sensorspp::Sensor *q) :
		q(q),
        pluginspath_(PLUGIN_PREFIX){
		using namespace std::experimental::filesystem;
        loader_.load(path(pluginspath_));
	}
	~SensorPrivate() {}
	using FeaturesFunc = std::function<Sensorspp::Features(const sensors_chip_name *name, Sensorspp::Chip &chip)>;
	std::tuple<Sensorspp::Chips::iterator, bool> chip_search(const char *name) {
		auto iter = std::find_if(chips_.begin(), chips_.end(), [name](const Sensorspp::Chip &nxt) {
			return nxt.name == name;
		});
        return std::make_tuple(iter, iter != chips_.end() ? true : false);
    };
    std::tuple<Sensorspp::ChipPluginsBase::const_iterator, bool> plugin_search(const char *name) {
        auto &plugins = loader_.plugins();
        auto iter = std::find_if(plugins.begin(), plugins.end(), [name](const Sensorspp::ChipPluginBasePtr &nxt) {
            return nxt->name() == name;
        });
        return std::make_tuple(iter, iter != plugins.end() ? true : false);
    };
private:
	void refresh(const char *chipstring, FeaturesFunc featuresfunc) {
		if (!chipstring) {
			update_all_chips(featuresfunc);
		} else {
			update_specific_chip(chipstring, featuresfunc);
		}
	}
	void serialize(std::ostream &os) {
		os << "{\"chips\":[";
		for (Sensorspp::Chip &chip : chips_) {
			os << chip << std::endl;
			if (&chip != &chips_.back()) {
				os << ",";
			}
		}
		os << "]}";
	}
private:
	//Helper functions
	Sensorspp::Chip cppchip(const sensors_chip_name *chip, FeaturesFunc featuresfunc = nullptr) {
		Sensorspp::Chip result;
		result.prefix = chip->prefix;
		result.name = sprintf_chip_name(chip);
		result.addr = chip->addr;
		result.path = chip->path;
		//Pass Temp feature directly to the Chip
		result.features = featuresfunc(chip, result);

		return result;
	}
	Sensorspp::Features cppfeatures(const sensors_chip_name *name, Sensorspp::Chip &chip) {
			Sensorspp::Features result_features;

			const sensors_feature *feature;
			int i(0);
			while ((feature = sensors_get_features(name, &i))) {
				switch (feature->type) {
				case SENSORS_FEATURE_TEMP: {
					Sensorspp::FeaturePtr featurep = cpptemp(name, feature);
					chip.temps.push_back(*static_cast<Sensorspp::Temp*>(cpptemp(name, feature).get()));
				}
				break;
				case SENSORS_FEATURE_IN:
					result_features.push_back(cppin(name, feature));
					break;
				case SENSORS_FEATURE_FAN:
					result_features.push_back(cppfan(name, feature));
					break;
				case SENSORS_FEATURE_VID:
					result_features.push_back(cppvid(name, feature));
					break;
				case SENSORS_FEATURE_BEEP_ENABLE:
					result_features.push_back(cppbeepenable(name, feature));
					break;
				case SENSORS_FEATURE_POWER:
					result_features.push_back(cpppower(name, feature));
					break;
				case SENSORS_FEATURE_ENERGY:
					result_features.push_back(cppenergy(name, feature));
					break;
				case SENSORS_FEATURE_CURR:
					result_features.push_back(cppcurr(name, feature));
					break;
				case SENSORS_FEATURE_INTRUSION:
					result_features.push_back(cppintrusion(name, feature));
					break;
				case SENSORS_FEATURE_HUMIDITY:
					result_features.push_back(cpphumidity(name, feature));
					break;
				default:
					continue;
				}
			}
			return result_features;
	}
	//Update only temperature feature
	Sensorspp::Features cppfeaturestemp(const sensors_chip_name *name, Sensorspp::Chip &chip) {
		Sensorspp::Features result_features;

		const sensors_feature *feature;
		int i(0);
		bool interrupt(false);
		while ((feature = sensors_get_features(name, &i)) && !interrupt) {
			switch (feature->type) {
			case SENSORS_FEATURE_TEMP: {
				Sensorspp::FeaturePtr featurep = cpptemp(name, feature);
				chip.temps.push_back(*static_cast<Sensorspp::Temp*>(cpptemp(name, feature).get()));
				interrupt = true;
				break;
			}
			default:
				continue;
			}
		}
		return result_features;
	}

	Sensorspp::FeaturePtr cpptemp(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Temp> result(new Sensorspp::Temp);

		struct sensor_subfeature_data sensors[NUM_TEMP_SENSORS];
		struct sensor_subfeature_data alarms[NUM_TEMP_ALARMS];
		int sensor_count, alarm_count;
		const sensors_subfeature *sf;
		double val;
		char *label;
		int i;

		if (!(label = sensors_get_label(name, feature))) {
			throw Sensorspp::GetLabelError("ERROR: Can't get label of feature" + std::string(feature->name));
		}

		result->label = label;
		free(label);

		sf = sensors_get_subfeature(name, feature,
				SENSORS_SUBFEATURE_TEMP_FAULT);
		if (sf && get_value(name, sf)) {
			printf("   FAULT  ");
		} else {
			sf = sensors_get_subfeature(name, feature,
					SENSORS_SUBFEATURE_TEMP_INPUT);
			if (sf && get_input_value(name, sf, &val) == 0) {
				get_input_value(name, sf, &val);
				result->value = val;
			} else
				result->value = -1;
		}

		sensor_count = alarm_count = 0;
		get_sensor_limit_data(name, feature, temp_sensors,
				sensors, &sensor_count, alarms, &alarm_count,
				result->subsensors, result->subalarms);

		/* print out temperature sensor info */
		sf = sensors_get_subfeature(name, feature,
				SENSORS_SUBFEATURE_TEMP_TYPE);
		if (sf) {
			int sens = (int)get_value(name, sf);
			/* older kernels / drivers sometimes report a beta value for
							   thermistors */
			if (sens > 1000)
				sens = 4;
			result->sensor = (sens == 0 ? "disabled" :
					sens == 1 ? "CPU diode" :
					sens == 2 ? "transistor" :
					sens == 3 ? "thermal diode" :
					sens == 4 ? "thermistor" :
					sens == 5 ? "AMD AMDSI" :
					sens == 6 ? "Intel PECI" : "unknown");
		}
		return result;
	}
	Sensorspp::FeaturePtr cppin(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::In> result(new Sensorspp::In);

		const sensors_subfeature *sf;
		char *label;
		struct sensor_subfeature_data sensors[NUM_IN_SENSORS];
		struct sensor_subfeature_data alarms[NUM_IN_ALARMS];
		int sensor_count, alarm_count;
		double val;

		if (!(label = sensors_get_label(name, feature))) {
			throw Sensorspp::GetLabelError("ERROR: Can't get label of feature" + std::string(feature->name));
		}
		result->label = label;
		free(label);

		sf = sensors_get_subfeature(name, feature,
				SENSORS_SUBFEATURE_IN_INPUT);
		if (sf && get_input_value(name, sf, &val) == 0)
			result->value = val;
		else
			result->value = -1;

		sensor_count = alarm_count = 0;
		get_sensor_limit_data(name, feature, voltage_sensors,
				sensors, &sensor_count, alarms, &alarm_count,
				result->subsensors, result->subalarms);

		return result;
	}
	Sensorspp::FeaturePtr cppfan(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Fan> result(new Sensorspp::Fan);

		const sensors_subfeature *sf, *sfmin, *sfmax, *sfdiv;
		double val;
		char *label;

		if (!(label = sensors_get_label(name, feature))) {
			throw Sensorspp::GetLabelError("ERROR: Can't get label of feature" + std::string(feature->name));
		}
		result->label = label;
		free(label);

		sf = sensors_get_subfeature(name, feature,
					    SENSORS_SUBFEATURE_FAN_FAULT);
		if (sf && get_value(name, sf))
			result->value = -1;
		else {
			sf = sensors_get_subfeature(name, feature,
						    SENSORS_SUBFEATURE_FAN_INPUT);
			if (sf && get_input_value(name, sf, &val) == 0)
				result->value = val;
			else
				result->value = -1;
		}

		sfmin = sensors_get_subfeature(name, feature,
					       SENSORS_SUBFEATURE_FAN_MIN);
		sfmax = sensors_get_subfeature(name, feature,
					       SENSORS_SUBFEATURE_FAN_MAX);
		sfdiv = sensors_get_subfeature(name, feature,
					       SENSORS_SUBFEATURE_FAN_DIV);
		if (sfmin || sfmax || sfdiv) {
			if (sfmin)
				result->min = get_value(name, sfmin);
			if (sfmax)
				result->max = get_value(name, sfmax);
			if (sfdiv)
				result->div = get_value(name, sfdiv);
		}
		sf = sensors_get_subfeature(name, feature,
					    SENSORS_SUBFEATURE_FAN_ALARM);
		sfmin = sensors_get_subfeature(name, feature,
					       SENSORS_SUBFEATURE_FAN_MIN_ALARM);
		sfmax = sensors_get_subfeature(name, feature,
					       SENSORS_SUBFEATURE_FAN_MAX_ALARM);
		if ((sf && get_value(name, sf)) ||
		    (sfmin && get_value(name, sfmin)) ||
		    (sfmax && get_value(name, sfmax)))
			result->alarm = true;

		return result;
	}
	Sensorspp::FeaturePtr cppvid(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Vid> result(new Sensorspp::Vid);

		char *label;
		const sensors_subfeature *subfeature;
		double vid;

		subfeature = sensors_get_subfeature(name, feature,
						    SENSORS_SUBFEATURE_VID);
		//Subfeature does not exist, not an exception problem, just continue
		if (!subfeature)
			return result;

		if ((label = sensors_get_label(name, feature))
		 && !sensors_get_value(name, subfeature->number, &vid)) {
			result->label = label;
			result->value = vid;
		}
		free(label);

		return result;
	}
	Sensorspp::FeaturePtr cppbeepenable(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::BeepEnable> result(new Sensorspp::BeepEnable);

		char *label;
		const sensors_subfeature *subfeature;
		double beep_enable;

		subfeature = sensors_get_subfeature(name, feature,
						    SENSORS_SUBFEATURE_BEEP_ENABLE);
		if (!subfeature)
			return result;

		if ((label = sensors_get_label(name, feature))
		 && !sensors_get_value(name, subfeature->number, &beep_enable)) {
			result->label = label;
			result->value = beep_enable ? "enabled" : "disabled";
		}
		free(label);

		return result;
	}

	#define NUM_POWER_ALARMS	4
	#define NUM_POWER_SENSORS	(ARRAY_SIZE(power_common_sensors) \
					 + ARRAY_SIZE(power_inst_sensors) \
					 - NUM_POWER_ALARMS - 2)

	Sensorspp::FeaturePtr cpppower(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Power> result(new Sensorspp::Power);

		double val;
		const sensors_subfeature *sf;
		struct sensor_subfeature_data sensors[NUM_POWER_SENSORS];
		struct sensor_subfeature_data alarms[NUM_POWER_ALARMS];
		int sensor_count, alarm_count;
		char *label;
		const char *unit;
		int i;

		if (!(label = sensors_get_label(name, feature))) {
			throw Sensorspp::GetLabelError("ERROR: Can't get label of feature" + std::string(feature->name));
		}
		result->label = label;
		free(label);

		sensor_count = alarm_count = 0;

		/*
		 * Power sensors come in 2 flavors: instantaneous and averaged.
		 * Most devices only support one flavor, so we try to display the
		 * average power if the instantaneous power attribute does not exist.
		 * If both instantaneous power and average power are supported,
		 * average power is displayed as limit.
		 */
		sf = sensors_get_subfeature(name, feature,
				SENSORS_SUBFEATURE_POWER_INPUT);
		get_sensor_limit_data(name, feature,
				sf ? power_inst_sensors : power_avg_sensors,
						sensors, &sensor_count, alarms, &alarm_count,
						result->subsensors, result->subalarms);
		/* Add sensors common to both flavors. */
		get_sensor_limit_data(name, feature, power_common_sensors,
				sensors, &sensor_count, alarms, &alarm_count,
				result->subsensors, result->subalarms);
		if (!sf)
			sf = sensors_get_subfeature(name, feature,
					SENSORS_SUBFEATURE_POWER_AVERAGE);

		if (sf && get_input_value(name, sf, &val) == 0) {
			scale_value(&val, &unit);
			result->unit = unit;
			result->value = val;
		} else
			result->value = -1;

		for (i = 0; i < sensor_count; i++) {
			/*
			 * Unit is W and needs to be scaled for all attributes except
			 * interval, which does not need to be scaled and is reported in
			 * seconds.
			 */
			if (strcmp(sensors[i].name, "interval")) {
				char *tmpstr;

				tmpstr = (char*)alloca(4);
				scale_value(&(result->subsensors[i].value), &unit);
				snprintf(tmpstr, 4, "%sW", unit);
				result->subsensors[i].unit = tmpstr;
			} else {
				result->subsensors[i].unit = "s";
			}
		}

		return result;
	}
	Sensorspp::FeaturePtr cppenergy(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Energy> result(new Sensorspp::Energy);

		double val;
			const sensors_subfeature *sf;
			char *label;
			const char *unit;

			if (!(label = sensors_get_label(name, feature))) {
				throw Sensorspp::GetLabelError("ERROR: Can't get label of feature" + std::string(feature->name));
			}
			result->label = label;
			free(label);

			sf = sensors_get_subfeature(name, feature,
						    SENSORS_SUBFEATURE_ENERGY_INPUT);
			if (sf && get_input_value(name, sf, &val) == 0) {
				scale_value(&val, &unit);
				result->value = val;
				result->unit = unit;
			} else
				result->value = -1;

		return result;
	}
	Sensorspp::FeaturePtr cppcurr(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Curr> result(new Sensorspp::Curr);

		const sensors_subfeature *sf;
		double val;
		char *label;
		struct sensor_subfeature_data sensors[NUM_CURR_SENSORS];
		struct sensor_subfeature_data alarms[NUM_CURR_ALARMS];
		int sensor_count, alarm_count;

		if (!(label = sensors_get_label(name, feature))) {
			throw Sensorspp::GetLabelError("ERROR: Can't get label of feature" + std::string(feature->name));
		}
		result->label = label;
		free(label);

		sf = sensors_get_subfeature(name, feature,
					    SENSORS_SUBFEATURE_CURR_INPUT);
		if (sf && get_input_value(name, sf, &val) == 0)
			result->value = val;
		else
			result->value = -1;

		sensor_count = alarm_count = 0;
		get_sensor_limit_data(name, feature, current_sensors,
				      sensors, &sensor_count, alarms, &alarm_count,
					  result->subsensors, result->subalarms);

		return result;
	}
	Sensorspp::FeaturePtr cppintrusion(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Intrusion> result(new Sensorspp::Intrusion);

		char *label;
		const sensors_subfeature *subfeature;
		double alarm;

		subfeature = sensors_get_subfeature(name, feature,
						    SENSORS_SUBFEATURE_INTRUSION_ALARM);
		if (!subfeature)
			return result;

		if ((label = sensors_get_label(name, feature))
		 && !sensors_get_value(name, subfeature->number, &alarm)) {
			result->label = label;
			result->value = alarm ? "ALARM" : "OK";
		}
		free(label);

		return result;
	}
	Sensorspp::FeaturePtr cpphumidity(const sensors_chip_name *name, const sensors_feature *feature) {
		std::shared_ptr<Sensorspp::Humidity> result(new Sensorspp::Humidity);

		char *label;
		const sensors_subfeature *subfeature;
		double humidity;

		subfeature = sensors_get_subfeature(name, feature,
				SENSORS_SUBFEATURE_HUMIDITY_INPUT);
		if (!subfeature)
			return result;

		if ((label = sensors_get_label(name, feature))
				&& !sensors_get_value(name, subfeature->number, &humidity)) {
			result->label = label;
			result->value = humidity;
		}
		free(label);

		return result;
	}

	/*
	 * Get sensor limit information.
	 * *num_limits and *num_alarms must be initialized by the caller.
	 */
	static void get_sensor_limit_data(const sensors_chip_name *name,
					  const sensors_feature *feature,
					  const struct sensor_subfeature_list *sfl,
					  struct sensor_subfeature_data *limits,
					  int *num_limits,
					  struct sensor_subfeature_data *alarms,
					  int *num_alarms,
					  Sensorspp::SubSensors &subsensors,
					  Sensorspp::SubAlarms &subalarms)
	{
		const sensors_subfeature *sf;

		for (; sfl->subfeature >= 0; sfl++) {
			sf = sensors_get_subfeature(name, feature, static_cast<sensors_subfeature_type>(sfl->subfeature));
			if (sf) {
				if (sfl->alarm) {
					/*
					 * Only queue alarm subfeatures if the alarm
					 * is active, and don't store the alarm value
					 * (it is implied to be active if queued).
					 */
					if (get_value(name, sf)) {
						alarms[*num_alarms].name = sfl->name;
						subalarms.push_back({sfl->name});
						(*num_alarms)++;
					}
				} else {
					/*
					 * Always queue limit subfeatures with their value.Re
					 */
					limits[*num_limits].value = get_value(name, sf);
					limits[*num_limits].name = sfl->name;
					subsensors.push_back({name: sfl->name, value: limits[*num_limits].value});
					(*num_limits)++;
				}
				if (sfl->exists) {
					get_sensor_limit_data(name, feature, sfl->exists,
							      limits, num_limits,
							      alarms, num_alarms,
								  subsensors,
								  subalarms);
				}
			}
		}
	}
	void update_all_chips(FeaturesFunc featuresfunc) {
		SensorRaiiHelper sensors_acquisitioner;
		const sensors_chip_name *chip;
		int chip_nr = 0;
		Sensorspp::Chips chips_replace;
		while ((chip = sensors_get_detected_chips(nullptr, &chip_nr))) {
			chips_replace.push_back(cppchip(chip, featuresfunc));
		}
		for (const auto &plugin : loader_.plugins()) {
		    chips_replace.emplace_back(plugin->chip());
		}
		chips_.swap(chips_replace);
	}
	Sensorspp::Chip get_chip(const char *chipstring, FeaturesFunc featuresfunc) {
        auto [iter, success] = plugin_search(chipstring);
        if (success) {
            return (*iter)->chip();
        }
        SensorRaiiHelper sensors_acquisitioner;
        const sensors_chip_name *chip;
        int chip_nr = 0;
        SensorsParseChipNameRaiiHelper chip_name_acq(chipstring);
        while ((chip = sensors_get_detected_chips(&chip_name_acq.parsed_chip, &chip_nr))) {
            return cppchip(chip, featuresfunc);
        }
        return Sensorspp::Chip();
	}
	void update_specific_chip(const char *chipstring, FeaturesFunc featuresfunc) {
	    using namespace Sensorspp;
		using Sensorspp::Chip;
        auto [iter, success] = chip_search(chipstring);
        auto updated_chip(get_chip(chipstring, featuresfunc));
		if (success) {
			std::swap(*iter, updated_chip);
		} else {
			chips_.push_back(std::move(updated_chip));
		}
	}
	static double get_value(const sensors_chip_name *name,
				const sensors_subfeature *sub)
	{
		double val;
		int err;

		err = sensors_get_value(name, sub->number, &val);
		if (err) {
			throw Sensorspp::GetValueError("ERROR: Can't get value of subfeature " + std::string(sub->name) + " : " +  sensors_strerror(err));
		}
		return val;
	}

	/* A variant for input values, where we want to handle errors gracefully */
	static int get_input_value(const sensors_chip_name *name,
				   const sensors_subfeature *sub,
				   double *val)
	{
		int err;

		err = sensors_get_value(name, sub->number, val);
		if (err && err != -SENSORS_ERR_ACCESS_R) {
			throw Sensorspp::GetValueError("ERROR: Can't get value of subfeature " + std::string(sub->name) + " : " +  sensors_strerror(err));
		}
		return err;
	}
	static const char *sprintf_chip_name(const sensors_chip_name *name)
	{
		constexpr const short buf_size =  200;
		static char buf[buf_size];

		if (sensors_snprintf_chip_name(buf, buf_size, name) < 0)
			return NULL;
		return buf;
	}
private:
	Sensorspp::Sensor *q;

	Sensorspp::Chips chips_;
    Sensorspp::PluginLoader<void> loader_;
    std::string pluginspath_;
};

} // namespace Private

namespace Sensorspp {
std::ostream& operator<<(std::ostream& os, const SubSensor& print) {
	os << "{"
			<< "\"name\":" << "\"" << print.name << "\","
			<< "\"value\":" << print.value << ","
			<< "\"unit\":" << "\"" << print.unit << "\""
			<< "}";
	return os;
}
std::ostream& operator<<(std::ostream& os, const SubAlarm& print) {
	os << "}"
			<< "\"name\":" << "\"" << print.name << "\""
			<< "}";
	return os;
}

void Temp::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"Temp\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" <<  value  << ","
			<< "\"subsensors\":[";
	for (const SubSensor &sub : subsensors) {
		os << sub;
		if (&sub != &subsensors.back()) {
			os << ",";
		} else {
			//std::cout << "sub: " << &sub << "; back: " << &subsensors.back() << std::endl;
		}
	}
	os << "],";
	os << "\"subalarms\":[";
	for (const SubAlarm &sub : subalarms) {
		os << sub;
		if (&sub != &subalarms.back()) {
			os << ",";
		}
	}
	os << "]}";
}
std::ostream & operator<<(std::ostream &os, const Temp &temp) {
	temp.serialize(os);
	return os;
}
std::ostream & operator<<(std::ostream &os, const Temps &temps) {
	os << "[";
	for (const Temp &sub : temps) {
		os << sub;
		if (&sub != &temps.back()) {
			os << ",";
		}
	}
	os << "]";
	return os;
}
void In::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"In\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << value << ","
			<< "\"subsensors\": [";
	for (const SubSensor &sub : subsensors) {
		os << sub;
		if (&sub != &subsensors.back()) {
			os << ",";
		}
	}
	os << "],";
	os << "\"subalarms\":[";
	for (const SubAlarm &sub : subalarms) {
		os << sub;
		if (&sub != &subalarms.back()) {
			os << ",";
		}
	}
	os << "]}";
}
void Fan::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"Fan\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << value << ","
			<< "\"alarm\":" << alarm << ","
			<< "\"min\":" << min << ","
			<< "\"max\":" << max << ","
			<< "\"div\":" << div
			<< "}";
}
void Vid::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"Vid\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << value
			<< "}";
}
void BeepEnable::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"BeepEnable\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << "\"" << value << "\""
			<< "}";
}
void Power::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"Power\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << value << ","
			<< "\"unit\":" << "\"" << unit << "\"" << ","
			<< "\"subsensors\": [";
	for (const SubSensor sub : subsensors) {
		os << sub;
		if (&sub != &subsensors.back()) {
			os << ",";
		}
	}
	os << "],";
	os << "\"subalarms\": [";
	for (const SubAlarm sub : subalarms) {
		os << sub;
		if (&sub != &subalarms.back()) {
			os << ",";
		}
	}
	os << "]}";
}
void Energy::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"Energy\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << value << ","
			<< "\"unit\":" << "\"" << unit << "\""
			<< "}";
}
void Curr::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"Curr\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << value << ","
			<< "\"subsensors\":[";
	for (const SubSensor sub : subsensors) {
		os << sub;
		if (&sub != &subsensors.back()) {
			os << ",";
		}
	}
	os << "],";
	os << "\"subalarms\": [";
	for (const SubAlarm sub : subalarms) {
		os << sub;
		if (&sub != &subalarms.back()) {
			os << ",";
		}
	}
	os << "]}";
}
void Intrusion::serialize(std::ostream &os) const {
	os << "{"
			<< "\"type\":\"Intrusion\","
			<< "\"label\": " << "\"" << label << "\"" << ","
			<< "\"value\": " << "\"" << value << "\""
			<< "}";
}
void Humidity::serialize(std::ostream &os) const {
	os << "Humidity:\n"
			<< "\"type\":\"Humidity\","
			<< "\"label\":" << "\"" << label << "\"" << ","
			<< "\"value\":" << value
			<< "}";
}

std::ostream& operator<<(std::ostream& os, const Chip& print) {
	os << "{"
			<< "\"prefix\":" << "\"" << print.prefix << "\"" << ","
			<< "\"name\":" << "\"" << print.name << "\"" << ","
			<< "\"addr\":" << "\"" << print.addr << "\"" << ","
			<< "\"path\":" << "\"" << print.path << "\"" << ","
			<< "\"temps\":" << print.temps << ",";
	os << "\"features\":" <<  "[";
	for (FeaturePtr feature : print.features) {
		feature->serialize(os);
		auto back = print.features.back();
		if (feature != back) {
			os << ",";
		}
	}
	os << "]}";

	return os;
}

std::ostream& operator<<(std::ostream& os, const Sensor& print) {
	print.Serialize(os);
	return os;
}

Sensor::Sensor() :
		d(new Private::SensorPrivate(this)) {
}
Sensor::~Sensor() {

}
void Sensor::Refresh(const char *chipstring) {
	using namespace Private;
	using namespace std::placeholders;
	SensorPrivate::FeaturesFunc f = std::bind(&SensorPrivate::cppfeatures, d.get(), _1, _2);
	d->refresh(chipstring, f);
}
void Sensor::RefreshTemp(const char *chipstring) {
	using namespace Private;
	using namespace std::placeholders;
	SensorPrivate::FeaturesFunc f = std::bind(&SensorPrivate::cppfeaturestemp, d.get(), _1,	_2);
	d->refresh(chipstring, f);
}
void Sensor::Serialize(std::ostream &os) const {
	d->serialize(os);
}

Chips Sensor::AllChips() const {
	return d->chips_;
}
std::string Sensor::PluginsPath() const {
    return d->pluginspath_;
}


} //namespace Sensorspp
