#include "Common.h"

namespace Sensorspp {

#ifdef __unix__
#include "dlfcn.h"

std::tuple<CreateChipPluginFunc, bool> readSymbols(const std::string &name) {
    CreateChipPluginFunc result_func;
    bool success(true);
    void *handle = dlopen(name.c_str(), RTLD_LAZY);
    char *error;
    *(void **) (&result_func) = dlsym(handle, "CreateObject");
    if ((error = dlerror()) != nullptr)  {
        fprintf(stderr, "%s\n", error); fflush(stderr);
        success = false;
        exit(EXIT_FAILURE);
    }
    return std::make_tuple(result_func, success);
}

#endif

} // namespace Sensorspp
