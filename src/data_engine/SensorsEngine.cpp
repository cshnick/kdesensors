/*
 * Copyright 2011 Aaron Seigo <aseigo@kde.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 *    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QColor>
#include <QTime>

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <fstream>
#include <utility>

//#include <klocalizedstring.h>

#include "SensorsEngine.h"
#include "Sensor.h"

#define s2q_(value) QString::fromStdString(value)
using EngineData = Plasma::DataEngine::Data;

class F {
public:
#ifndef SUPPRESS_LOG
	template <typename Arg, typename... Args>
	static void Log(Arg&& arg, Args&&... args) {
		fs.open("/tmp/hacklog", std::fstream::out | std::fstream::app);
	    fs << std::forward<Arg>(arg);
	    using expander = int[];
	    (void)expander{0, (void(fs << ' ' << std::forward<Args>(args)),0)...};
	    fs << std::endl;
	    fs.close();
	}
#else // #ifndef SUPPRESS_LOG
	template <typename Arg, typename... Args>
	static void Log(Arg&&, Args&&...) {

	}
#endif // #ifndef SUPPRESS_LOG
private:
	static std::fstream fs;
};
std::fstream F::fs("/tmp/hacklog");

class ConverterHelper {
public:
	static void chip_to_data(const Sensorspp::Chip &chip, EngineData &data) {
		//data.insert("prefix", QString::fromStdString(chip.prefix));
		data.insert("addr", chip.addr);
		data.insert("path", s2q_(chip.path));
		if (chip.temps.size()) {
			QVariantList temp_list;
			for (const auto &chip_temp : chip.temps) {
				EngineData temp;
				temp.insert("label", s2q_(chip_temp.label));
				temp.insert("value", chip_temp.value);
				temp_list.append(temp);
			}
			data.insert("temp", temp_list);
		}
	}
};

class SensorsEnginePrivate {
	friend class SensorsEngine;
public:
	SensorsEnginePrivate(SensorsEngine *q)
		: q(q) {
		sensor_.Refresh(nullptr);
		F::Log("================");
		F::Log("Initial SensorsEnginePrivate");
		auto chips = sensor_.AllChips();
		for (const auto &chip : chips) {
			EngineData tmp;
			ConverterHelper::chip_to_data(chip, tmp);
			QString chipname(s2q_(chip.name));
			q->setData(chipname, tmp);
			engine_data_.insert(chipname, tmp);
		}
	}
	~SensorsEnginePrivate() {
		F::Log("SensorsEnginePrivate destructor");
		F::Log("================");
	}

	void updateTemperature(const QString &source) {
		F::Log("Update temperature");
		sensor_.RefreshTemp(source.toUtf8().data());
		auto chips(sensor_.AllChips());
		for (const auto &chip: chips) {
			F::Log("\tChip: ", chip.name, "; source: ", source.toStdString());
			if (chip.name != source.toStdString()) {
				continue;
			}
			if (chip.temps.size()) {
				EngineData *srcptr = static_cast<EngineData*>(engine_data_[source].data());
				QVariantList *tempsptr = static_cast<QVariantList*>((*srcptr)["temp"].data());
				F::Log("\tPrinting list of temperatures");
				for (size_t i = 0; i < chip.temps.size(); i++) {
					auto chip_temp(chip.temps[i]);
					F::Log("\t\t", chip_temp.value);
					EngineData *temptr = static_cast<EngineData*>((*tempsptr)[i].data());
					(*temptr)["value"] = chip_temp.value;
				}
			}
		}
		q->setData(source, engine_data_[source].value<QVariantMap>());
	}

	bool sourceRequestEvent(const QString &source) {
		if (!engine_data_.contains(source)) {
			return false;
		}
		updateTemperature(source);
		return true;
	}

	bool updateSourceEvent(const QString &source) {
		updateTemperature(source);
		return true;
	}

private:
	SensorsEngine *q;
	Sensorspp::Sensor sensor_;
	Plasma::DataEngine::Data engine_data_;
};

SensorsEngine::SensorsEngine(QObject *parent, const QVariantList &args)
    : Plasma::DataEngine(parent, args)
{
	d.reset(new SensorsEnginePrivate(this));
}
SensorsEngine::~SensorsEngine() {}

bool SensorsEngine::sourceRequestEvent(const QString &source) {
	return d->sourceRequestEvent(source);
}

bool SensorsEngine::updateSourceEvent(const QString &source) {
	return d->updateSourceEvent(source);
}

// export the plugin; use the plugin name and the class name
K_EXPORT_PLASMA_DATAENGINE_WITH_JSON(org.cshnick.sensors_ex, SensorsEngine, "plasma-dataengine-sensors_ex.json")

// include the moc file so the build system makes it for us
#include "SensorsEngine.moc"

