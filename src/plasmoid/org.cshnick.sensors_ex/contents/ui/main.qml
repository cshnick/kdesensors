/*
 *  Copyright 2013 Marco Martin <mart@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 */

import QtQuick 2.0
import org.kde.plasma.plasmoid 2.0
import QtQuick.Layouts 1.0

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

Rectangle {
    id: root
    color: "transparent"
    width: 100
    height: 100
    radius: 10
    smooth: true
    Layout.minimumWidth: units.gridUnit * 20
    Layout.minimumHeight: column.implicitHeight
    
    property string cpuSensor: plasmoid.configuration.CpuSensor || 'k10temp-pci-00c3'
    property string gpuSensor: plasmoid.configuration.gpuSensor || 'nvidia-nvml'
    
    property var cpuSensorData: cpuSensor && dataSource.data[cpuSensor] || ({})
    property var gpuSensorData: gpuSensor && dataSource.data[gpuSensor] || ({})
    
    PlasmaCore.DataSource {
        id: dataSource
        engine: "org.cshnick.sensors_ex"
        connectedSources: sources
        interval: 1000
        Component.onCompleted: {
            plasmoid.configuration.Sources = sources
        }
    }

    Plasmoid.compactRepresentation: Component {
        CompactWidget {
            Layout.maximumWidth : 25
            cpuTemp: cpuSensorData &&
                     cpuSensorData.temp &&
                     cpuSensorData.temp[0] &&
                     parseInt(cpuSensorData.temp[0].value) ||
                     'n/a'
            gpuTemp: gpuSensorData &&
                     gpuSensorData.temp &&
                     gpuSensorData.temp[0] &&
                     parseInt(gpuSensorData.temp[0].value) ||
                     'n/a'
            //color: "#00aaff"
        }
    }

    PlasmaExtras.ConditionalLoader {
        anchors.fill: parent
        when: plasmoid.expanded
        source: Component { 
            Rectangle {
                Layout.minimumWidth : 150
                Layout.minimumHeight: 200
                anchors.fill : parent
                color: "yellow"
            }
        }
    }
    
    Component.onCompleted: {
        print("Test Applet loaded")
    }
}
