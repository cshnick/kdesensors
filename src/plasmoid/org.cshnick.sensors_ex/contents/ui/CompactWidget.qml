import QtQuick 2.5

import QtQuick.Layouts 1.3 as L

import org.kde.plasma.core 2.0 as Core
import org.kde.plasma.components 2.0 as Comps

Rectangle {
    property string cpuTemp
    property string gpuTemp

    Column {
        anchors.fill: parent
        Comps.Label {
            width: parent.width
            height: parent.height / 2
            text: cpuTemp || '01'
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }
        Comps.Label {
            width: parent.width
            height: parent.height / 2
            text: gpuTemp || '02'
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }
    }
    color: 'transparent'
    border.width: 0
    border.color: 'darkgrey'
}
