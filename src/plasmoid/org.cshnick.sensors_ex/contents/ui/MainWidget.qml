import QtQuick 2.5

import QtQuick.Layouts 1.3 as L
import org.kde.plasma.core 2.0 as Core
import org.kde.plasma.components 2.0 as Comps

Item {
    id: root

    property var cpuSensorData: ({})
    property var gpuSensorData: ({})

    L.ColumnLayout {
        spacing: 5
        anchors.fill: parent
        Comps.Label {
            id: textEdit
            L.Layout.preferredHeight: 40
            L.Layout.columnSpan: 0
            text: "Coming soon"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Item {
            id: _span

            width: 1
            height: 1
            L.Layout.fillHeight: true
        }
    }
}
