import QtQuick 2.4
import QtQuick.Layouts 1.3 as L
import QtQuick.Controls 1.3 as C
import QtQuick.Controls.Styles 1.3 as S
import org.kde.plasma.components 2.0 as Comps
import QtQuick.Controls.Styles.Plasma 2.0 as CS

Rectangle {
    id: _root

    property var model: ({})
    property var sources: plasmoid.configuration.Sources
    property string cpuSensor: _cpuCombo.currentText
    property string gpuSensor: _gpuCombo.currentText
    
    property alias cfg_CpuSensor: _cpuCombo.currentText
    property alias cfg_GpuSensor: _gpuCombo.currentText
     
    property int pheight : 30

    L.ColumnLayout {
        anchors.fill: parent
        //columns: 3
        clip: true
        Repeater {
            model: sources
            L.RowLayout {
                spacing: 0
                C.CheckBox {
                    L.Layout.preferredHeight: pheight
                    L.Layout.fillHeight: false
                    L.Layout.preferredWidth: 30
                    L.Layout.column: 0
                    L.Layout.row: index
                    width: 50
                    height: 50
                    style: S.CheckBoxStyle {
                        indicator: Rectangle {
                            implicitWidth: 16
                            implicitHeight: 16
                            radius: 0
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Rectangle {
                                visible: control.checked
                                color: "#555"
                                border.color: "#333"
                                radius: 1
                                anchors.margins: 4
                                anchors.fill: parent
                            }
                        }
                    }

                    //enabled: false
                    //L.Layout.rowSpan: index
                }
                C.Label {
                    L.Layout.preferredHeight: pheight
                    L.Layout.fillHeight: false
                    L.Layout.column: 1
                    L.Layout.row: index
                    verticalAlignment: Text.AlignVCenter
                    //L.Layout.rowSpan: index
                    text: sources[index] + ' - '
                    enabled: true
                }
                C.TextField {
                    L.Layout.preferredHeight: pheight
                    L.Layout.fillHeight: false
                    L.Layout.column: 2
                    L.Layout.row: index
                    L.Layout.fillWidth: true
                    verticalAlignment: Text.AlignVCenter
                    text: ""
                    textColor: '#333'
                    placeholderText: "Alias..."
                    enabled: true
                    style: CS.TextFieldStyle {
                        background: Rectangle {
                            radius: 0
                            border.color: "#333"
                            border.width: 0
                            color: "transparent"
                        }
                        placeholderTextColor: '#888'
                    }
                    Component.onCompleted: {
                        console.log('Name of the component')
                    }
                    onAccepted: {
                        globalSettings.sources[modelData] = text
                        globalSettings.sources = globalSettings.sources
                        console.log("onAccepted")
                    }
                }
            }
        }
        Rectangle {
            color: "transparent"
            L.Layout.columnSpan: 3
            L.Layout.preferredHeight: 500
            L.Layout.preferredWidth: parent.width
            Column {
                Rectangle {
                    color: "transparent"
                    height: 20
                    width: parent.width
                }
                Grid {
                    spacing: 10
                    columns: 2
                    C.Label {text: "Cpu temp"; height: pheight; verticalAlignment: Text.AlignVCenter}
                    CustomCombo {
                        id: _cpuCombo; model: sources; currentIndex: -1; width: 150; height: pheight
                        onActivated: {
                            cpuSensorActivated(textAt(index))
                            plasmoid.configuration.CpuSensor =textAt(index)
                        }
                        Component.onCompleted: {
                            if (globalSettings && globalSettings.cpuSensor) {
                                currentIndex = find(plasmoid.configuration.CpuSensor)
                            }
                        }
                    }
                    C.Label {text: "Gpu temp"; height: pheight; verticalAlignment: Text.AlignVCenter}
                    CustomCombo {
                        id: _gpuCombo; model: sources; currentIndex: 0; width: 150; height: pheight
                        onActivated: {
                            gpuSensorActivated(textAt(index))
                            plasmoid.configuration.GpuSensor =textAt(index)
                        }
                        Component.onCompleted: {
                            if (globalSettings && globalSettings.gpuSensor) {
                                currentIndex = find(plasmoid.configuration.GpuSensor)
                            }
                        }
                    }
                }
            }
        }
    }

    color: 'transparent'
}
