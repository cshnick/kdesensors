#include "ChipPluginNvidia.h"

#include "nvml.h"

namespace Sensorspp {

Chip ChipPluginNvidia::chip() {
    Chip result;
    result.prefix = Name::nvidia_nvml;
    result.name = Name::nvidia_nvml;
    result.addr = 0;

    nvmlReturn_t nvml_result;
    unsigned int temp;

    // First initialize NVML library
    nvml_result = nvmlInit();
    if (NVML_SUCCESS != nvml_result) {
        throw NVMLError(std::string("Failed to initialize NVML: ") + nvmlErrorString(nvml_result));
    }
    nvmlDevice_t device;
    nvml_result = nvmlDeviceGetHandleByIndex(0, &device);
    if (NVML_SUCCESS != nvml_result) {
        throw NVMLError(std::string("Failed to get handle for device 0: ") + nvmlErrorString(nvml_result));
    }
    nvml_result = nvmlDeviceGetTemperature(device, NVML_TEMPERATURE_GPU, &temp);
    if (NVML_SUCCESS != nvml_result) {
        throw NVMLError(
                std::string("Failed to get temperature of device 0: ") + nvmlErrorString(nvml_result));
    }

    std::shared_ptr<Temp> chip_temp(new Temp);
    chip_temp->label = "initial";
    chip_temp->value = temp;

    result.temps.push_back(*chip_temp);

    nvml_result = nvmlShutdown();
    if (NVML_SUCCESS != nvml_result) {
        throw NVMLError(std::string("Failed to shutdown NVML: ") + nvmlErrorString(nvml_result));
    }

    return result;
}

extern "C"
ChipPluginBase *CreateObject() {
    return new ChipPluginNvidia;
}

} // namespace Sensorspp