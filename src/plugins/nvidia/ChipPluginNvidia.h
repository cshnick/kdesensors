#pragma once

#include "Sensor.h"
#include "Common.h"

namespace Sensorspp {

class ChipPluginNvidia final : public ChipPluginBase {
public:
    Chip chip() override;
    std::string name() override {return Name::nvidia_nvml;}
    std::string ownerinfo() override {return "Ilia Ryabokon";}
};

} // namespace Sensorspp
